Trip Sorter
===========

Running code
------------

BoardingCard, Location and process function are defined in the boarding_cards module.

Example::

   from boarding_cards import BoardingCard, Location, process, TravelPlan
   cards = [
      BoardingCard(start=Location("Warsaw"), destination=Location("Radom")),
      BoardingCard(start=Location("Radom"), destination=Location("Kraków")),
      BoardingCard(start=Location("Warsaw"), destination=Location("Kraków")),
      BoardingCard(start=Location("Kraków"), destination=Location("Zakopane")),
   ]
   process(cards)


Running tests
-------------

In order to run tests, use unittest::

   python -m unittest
