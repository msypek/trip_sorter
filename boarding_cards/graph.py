"""Module defining `Graph`, `Node`, and `Edge` classes that can be used to define general-purpose graphs."""
from typing import Any, Dict, List, Set

from dataclasses import dataclass, field


@dataclass(unsafe_hash=True)
class Node:
    _graph: "Graph" = field(repr=False, compare=False, hash=False)
    name: str = field(compare=True, hash=True)
    is_source: bool = field(default=True, repr=False, compare=False, hash=False)
    is_sink: bool = field(default=True, repr=False, compare=False, hash=False)
    incoming_edges: List["Edge"] = field(default_factory=list, repr=False, compare=False, hash=False)
    outgoing_edges: List["Edge"] = field(default_factory=list, repr=False, compare=False, hash=False)


@dataclass(unsafe_hash=True)
class Edge:
    """Directed edge.
    
    Generic data can be assigned to an edge using `related_object` attribute.
    """

    _graph: "Graph" = field(compare=False, hash=False)
    related_object: Any = field(compare=True, hash=True)
    start_node: Node = field(compare=True, hash=True)
    end_node: Node = field(compare=True, hash=True)

    def __init__(self, _graph, related_object, start_node, end_node):
        self._graph = _graph
        self.related_object = related_object
        self.start_node = start_node
        self.end_node = end_node
        self.start_node.outgoing_edges.append(self)
        self.end_node.incoming_edges.append(self)


class Graph:
    """Directed graph implementation."""

    _nodes_by_name: Dict[str, Node]
    _edges: Set[Edge]

    def __init__(self):
        self._nodes_by_name = {}
        self._edges = set()

    def add_node(self, name: str) -> Node:
        """Add node with a given name.

        This method is idempotent, and attempting to add another node with the same name will return
        previously added node.
        """
        if name not in self._nodes_by_name:
            self._nodes_by_name[name] = Node(self, name)
        return self._nodes_by_name[name]

    def add_edge(self, name_a: str, name_b: str, related_object: Any):
        node_a = self.add_node(name_a)
        node_b = self.add_node(name_b)
        node_a.is_sink = False
        node_b.is_source = False
        self._edges.add(Edge(self, related_object, node_a, node_b))

    def get_shortest_paths(self, source: Node) -> List[List[Edge]]:
        """Return list of shortest paths from provided `source` to destinations (nodes with is_sink==True).
        
        Uses Dijkstra's algorithm.
        """
        INFINITY = float('inf')
        dist: Dict[Node, int] = {}
        prev: Dict[Node, List[Edge]] = {}
        Q: Set[Node] = set()

        prev[source] = []
        dist[source] = 0

        for node in self._nodes_by_name.values():
            if node != source:
                dist[node] = INFINITY
            Q.add(node)

        while Q:
            # Obtain node from `Q` with the lowest dist value.
            u: Node = sorted(
                {x:y for x, y in dist.items() if x in Q}.items(),
                key=lambda x: x[1]
            )[0][0]
            Q.remove(u)

            for edge in u.outgoing_edges:
                v = edge.end_node
                alt = dist[u] + 1
                if alt < dist[v]:
                    dist[v] = alt
                    prev[v] = prev[u] + [edge]
        return [edges for node, edges in prev.items() if node.is_sink]
