from enum import Enum
from dataclasses import dataclass, field
from typing import List, Iterable, Optional

from .graph import Graph


class MeansOfTransportationEnum(Enum):

    FLIGHT = 1
    BUS = 2
    TRAIN = 3
    SHIP = 4


@dataclass(frozen=True)
class Location:

    name: str


@dataclass(frozen=True)
class BoardingCard:

    start: Location
    destination: Location
    means_of_transportation: MeansOfTransportationEnum = field(default=MeansOfTransportationEnum.TRAIN)
    seat_number: Optional[str] = None
    gate: Optional[str] = None


class TravelPlan:
    """Helper class used for storing sequences of BoardingCards.

    Mostly used in tests, to make make it possible to use `assertSetEqual` on list of BoardingCards, since regular lists are not hashable.
    """

    def __init__(self, boarding_cards: List[BoardingCard]) -> None:
        self.boarding_cards = boarding_cards

    def __hash__(self):
        return hash(x for x in self.boarding_cards)

    def __eq__(self, value):
        if isinstance(value, self.__class__):
            return self.boarding_cards == value.boarding_cards
        return False


def process(boarding_cards: Iterable[BoardingCard]) -> List[TravelPlan]:
    """Produce list of possible TravelPlans from set of BoardingCards.

    Builds digraph from provided boarding cards while determining which locations are sources
    and which destinations, and uses Dijkstra's algorithm to find shortest paths from sources
    to destinations.
    """
    graph = Graph()
    for boarding_card in boarding_cards:
        graph.add_node(boarding_card.start.name)
        graph.add_node(boarding_card.destination.name)
        graph.add_edge(boarding_card.start.name, boarding_card.destination.name, boarding_card)

    start_nodes = {node for node in graph._nodes_by_name.values() if node.is_source}
    if not start_nodes:
        return []
    paths = []
    for start_node in start_nodes:
        paths += graph.get_shortest_paths(start_node)
    return [TravelPlan([edge.related_object for edge in path]) for path in paths]
