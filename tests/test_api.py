from unittest import TestCase

from boarding_cards import BoardingCard, Location, TravelPlan, process


class ApiTestCase(TestCase):

    maxDiff = None

    def test_acyclic_digraph_with_alternate_path(self):
        cards = [
            BoardingCard(start=Location("Warsaw"), destination=Location("Radom")),
            BoardingCard(start=Location("Radom"), destination=Location("Kraków")),
            BoardingCard(start=Location("Warsaw"), destination=Location("Kraków")),
            BoardingCard(start=Location("Kraków"), destination=Location("Zakopane")),
        ]
        self.assertListEqual(
            process(cards),
            [TravelPlan([
                BoardingCard(start=Location("Warsaw"), destination=Location("Kraków")),
                BoardingCard(start=Location("Kraków"), destination=Location("Zakopane")),
            ])]
        )


    def test_cyclic_digraph(self):
        cards = [
            BoardingCard(start=Location("Warsaw"), destination=Location("Radom")),
            BoardingCard(start=Location("Radom"), destination=Location("Kraków")),
            BoardingCard(start=Location("Warsaw"), destination=Location("Kraków")),
            BoardingCard(start=Location("Kraków"), destination=Location("Lublin")),
            BoardingCard(start=Location("Lublin"), destination=Location("Radom")),
            BoardingCard(start=Location("Kraków"), destination=Location("Zakopane")),
        ]
        self.assertListEqual(
            process(cards),
            [TravelPlan([
                BoardingCard(start=Location("Warsaw"), destination=Location("Kraków")),
                BoardingCard(start=Location("Kraków"), destination=Location("Zakopane")),
            ])]
        )


    def test_digraph_without_deterministic_start_point(self):
        cards = [
            BoardingCard(start=Location("Warsaw"), destination=Location("Radom")),
            BoardingCard(start=Location("Radom"), destination=Location("Kraków")),
            BoardingCard(start=Location("Warsaw"), destination=Location("Kraków")),
            BoardingCard(start=Location("Kraków"), destination=Location("Lublin")),
            BoardingCard(start=Location("Lublin"), destination=Location("Warsaw")),
            BoardingCard(start=Location("Kraków"), destination=Location("Zakopane")),
        ]
        self.assertListEqual(
            process(cards),
            []
        )

    def test_acyclic_digraph_with_multiple_end_points(self):
        cards = [
            BoardingCard(start=Location("Warsaw"), destination=Location("Radom")),
            BoardingCard(start=Location("Radom"), destination=Location("Kraków")),
            BoardingCard(start=Location("Warsaw"), destination=Location("Kraków")),
            BoardingCard(start=Location("Kraków"), destination=Location("Lublin")),
            BoardingCard(start=Location("Lublin"), destination=Location("Białystok")),
            BoardingCard(start=Location("Kraków"), destination=Location("Zakopane")),
        ]
        self.assertSetEqual(
            set(process(cards)),
            {
                TravelPlan([
                    BoardingCard(start=Location("Warsaw"), destination=Location("Kraków")),
                    BoardingCard(start=Location("Kraków"), destination=Location("Zakopane")),
                ]),
                TravelPlan([
                    BoardingCard(start=Location("Warsaw"), destination=Location("Kraków")),
                    BoardingCard(start=Location("Kraków"), destination=Location("Lublin")),
                    BoardingCard(start=Location("Lublin"), destination=Location("Białystok")),
                ]),
            }
        )

    def test_acyclic_digraph_with_multiple_start_points(self):
        cards = [
            BoardingCard(start=Location("Warsaw"), destination=Location("Radom")),
            BoardingCard(start=Location("Radom"), destination=Location("Kraków")),
            BoardingCard(start=Location("Warsaw"), destination=Location("Kraków")),
            BoardingCard(start=Location("Białystok"), destination=Location("Lublin")),
            BoardingCard(start=Location("Lublin"), destination=Location("Kraków")),
            BoardingCard(start=Location("Kraków"), destination=Location("Zakopane")),
        ]
        self.assertSetEqual(
            set(process(cards)),
            {
                TravelPlan([
                    BoardingCard(start=Location("Białystok"), destination=Location("Lublin")),
                    BoardingCard(start=Location("Lublin"), destination=Location("Kraków")),
                    BoardingCard(start=Location("Kraków"), destination=Location("Zakopane")),
                ]),
                TravelPlan([
                    BoardingCard(start=Location("Warsaw"), destination=Location("Kraków")),
                    BoardingCard(start=Location("Kraków"), destination=Location("Zakopane")),
                ]),
            }
        )
